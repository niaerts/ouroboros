/*
 * Ouroboros - Copyright (C) 2016 - 2018
 *
 * Simple PDU Forwarding Function
 *
 *    Dimitri Staessens <dimitri.staessens@ugent.be>
 *    Sander Vrijders   <sander.vrijders@ugent.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., http://www.fsf.org/about/contact/.
 */

#define _POSIX_C_SOURCE 200112L

#define OUROBOROS_PREFIX "mp_pff"

#include "config.h"

#include <ouroboros/hashtable.h>
#include <ouroboros/logs.h>

#include <inttypes.h>
#include <assert.h>
#include <pthread.h>

#include "mp_pff.h"

struct pff_i {
    struct htable *table;
    pthread_rwlock_t lock;
};

struct pol_pff_ops mp_pff_ops = {
        .create            = mp_pff_create,
        .destroy           = mp_pff_destroy,
        .lock              = mp_pff_lock,
        .unlock            = mp_pff_unlock,
        .add               = mp_pff_add,
        .update            = mp_pff_update,
        .del               = mp_pff_del,
        .flush             = mp_pff_flush,
        .nhop              = mp_pff_nhop,
        .flow_state_change = NULL
};

struct pff_i *mp_pff_create(void) {
        struct pff_i *tmp;

        tmp = malloc(sizeof(*tmp));
        if (tmp == NULL)
                return NULL;

        if (pthread_rwlock_init(&tmp->lock, NULL)) {
                free(tmp);
                return NULL;
        }

        tmp->table = htable_create(PFT_SIZE, false);
        if (tmp->table == NULL) {
                pthread_rwlock_destroy(&tmp->lock);
                free(tmp);
                return NULL;
        }

        return tmp;
}

void list_destroy_free(void * ref){
        list_destroy(ref, NULL);
}

void mp_pff_destroy(struct pff_i *pff_i) {
        assert(pff_i);

        htable_destroy(pff_i->table, list_destroy_free);

        pthread_rwlock_destroy(&pff_i->lock);

        free(pff_i);
}

void mp_pff_lock(struct pff_i *pff_i) {
        pthread_rwlock_wrlock(&pff_i->lock);
}

void mp_pff_unlock(struct pff_i *pff_i) {
        pthread_rwlock_unlock(&pff_i->lock);
}

int mp_pff_add(struct pff_i *   pff_i,
               uint64_t         addr,
               int *            fd,
               int              fd_i,
               size_t           len) {
        struct list_head *f;
        struct fd *fds;
        size_t i = 0;
        size_t l_len;

        assert(pff_i);
        assert(len > 0);

        (void) fd_i;

        if (htable_lookup(pff_i->table, addr, (void **) &f, &l_len)) {
                fds = malloc(sizeof(*fds));

                list_head_init(&fds->next);

                f = &fds->next;

                fds->fd = fd[i++];

                if (f == NULL)
                        return -1;

                if (htable_insert(pff_i->table, addr, f, 1))
                        goto fail_f;
        }

        for (; i < len; i++) {
                fds = malloc(sizeof(*fds));

                list_head_init(&fds->next);

                fds->fd = fd[i];

                list_add_tail(f, &fds->next);
        }

        return 0;

        fail_f:
        free(f);
        return -1;
}

int mp_pff_update(struct pff_i *pff_i,
                  uint64_t      addr,
                  int *         fd,
                  int           fd_i,
                  size_t        len) {
        int r;

        assert(pff_i);
        assert(len > 0);

        (void) len;

        r = mp_pff_del(pff_i, addr);

        if (r != 0)
                return r;

        return mp_pff_add(pff_i, addr, fd, fd_i, len);
}

int mp_pff_del(struct pff_i *pff_i,
               uint64_t addr) {
        struct list_head *f;
        size_t len;

        assert(pff_i);

        if (htable_lookup(pff_i->table, addr, (void **) &f, &len)) {
                return -1;
        }

        /* TODO: Free list and all entries */

        if (htable_delete(pff_i->table, addr, list_destroy_free))
                return -1;

        return 0;
}

void mp_pff_flush(struct pff_i *pff_i) {
        assert(pff_i);

        htable_flush(pff_i->table, list_destroy_free);
}

int mp_pff_nhop(struct pff_i *pff_i,
                uint64_t addr,
                int fd_i) {
        struct list_head   * f;
        struct fd           * fds;
        struct htable_entry * entry;
        int                   fd = -1;

        (void) fd_i;

        assert(pff_i);

        pthread_rwlock_rdlock(&pff_i->lock);

        if (!htable_lookup_entry(pff_i->table, addr, &entry)) {
                f = (struct list_head *) entry->val;

                fds = list_entry(f->nxt, struct fd, next);

                entry->val = f->nxt;

                fd = fds->fd;
        }

        if(fd == -1)
                log_dbg("Could not find entry for: %" PRIu64, addr);

        pthread_rwlock_unlock(&pff_i->lock);

        return fd;
}
