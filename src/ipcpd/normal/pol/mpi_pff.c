/*
 * Ouroboros - Copyright (C) 2016 - 2018
 *
 * Simple PDU Forwarding Function
 *
 *    Dimitri Staessens <dimitri.staessens@ugent.be>
 *    Sander Vrijders   <sander.vrijders@ugent.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., http://www.fsf.org/about/contact/.
 */

#define _POSIX_C_SOURCE 200112L

#define OUROBOROS_PREFIX "mpi_pff"

#include "config.h"

#include <ouroboros/hashtable.h>
#include <ouroboros/logs.h>

#include <inttypes.h>
#include <assert.h>
#include <pthread.h>

#include "mp_pff.h"
#include "mpi_pff.h"

struct pff_i {
    struct htable *table;
    pthread_rwlock_t lock;
};

struct pol_pff_ops mpi_pff_ops = {
        .create            = mpi_pff_create,
        .destroy           = mpi_pff_destroy,
        .lock              = mpi_pff_lock,
        .unlock            = mpi_pff_unlock,
        .add               = mpi_pff_add,
        .update            = mpi_pff_update,
        .del               = mpi_pff_del,
        .flush             = mpi_pff_flush,
        .nhop              = mpi_pff_nhop,
        .flow_state_change = NULL
};

struct pff_i *mpi_pff_create(void) {
        struct pff_i *tmp;

        tmp = malloc(sizeof(*tmp));
        if (tmp == NULL)
                return NULL;

        if (pthread_rwlock_init(&tmp->lock, NULL)) {
                free(tmp);
                return NULL;
        }

        tmp->table = htable_create(PFT_SIZE, false);
        if (tmp->table == NULL) {
                pthread_rwlock_destroy(&tmp->lock);
                free(tmp);
                return NULL;
        }

        return tmp;
}

void mp_pff_destructor(void* ref){
        mp_pff_destroy(ref);
}

void mpi_pff_destroy(struct pff_i *pff_mp) {
        assert(pff_mp);

        htable_destroy(pff_mp->table, mp_pff_destructor);

        pthread_rwlock_destroy(&pff_mp->lock);
        free(pff_mp);
}

void mpi_pff_lock(struct pff_i *pff_mp) {
        pthread_rwlock_wrlock(&pff_mp->lock);
}

void mpi_pff_unlock(struct pff_i *pff_mp) {
        pthread_rwlock_unlock(&pff_mp->lock);
}

int mpi_pff_add(struct pff_i *  pff_mp,
                uint64_t        addr,
                int *           fd,
                int             fd_i,
                size_t          len) {
        struct pff_i * pff_h;
        size_t i = 0;
        size_t l_len;
        int r;

        assert(pff_mp);
        assert(len > 0); /* fd_i is incomming FD */

        if(fd_i < PROG_RES_FDS)
                fd_i = 0;

        if (htable_lookup(pff_mp->table, fd_i, (void **) &pff_h, &l_len)) {
                pff_h = mp_pff_create();

                if (pff_h == NULL)
                        return -1;

                if (htable_insert(pff_mp->table, fd_i, pff_h, 1))
                        goto fail_f;
        }

        for (; i < len; i++) {
                r = mp_pff_add(pff_h, addr, fd, fd_i, len);

                if(r)
                        return r;
        }

        return 0;

fail_f:
        mp_pff_destructor(pff_h);
        return -1;
}

int mpi_pff_update(struct pff_i *       pff_mp,
                   uint64_t             addr,
                   int *                fd,
                   int                  fd_i,
                   size_t               len) {
        struct pff_i * pff_h;
        size_t l_len;

        assert(pff_mp);
        assert(len > 0); /* fd[0] is incomming FD */

        (void) len;

        if(fd_i < PROG_RES_FDS)
                fd_i = 0;

        if (htable_lookup(pff_mp->table, fd_i, (void **) &pff_h, &l_len))
                return -1;

        return mp_pff_update(pff_h, addr, fd, fd_i, len);
}

int mpi_pff_del(struct pff_i *pff_mp,
               uint64_t addr) {
        struct pff_i *f;
        size_t len;

        assert(pff_mp);

        if (htable_lookup(pff_mp->table, addr, (void **) &f, &len)) {
                return -1;
        }

        if (htable_delete(pff_mp->table, addr, mp_pff_destructor))
                return -1;

        return 0;
}

void mpi_pff_flush(struct pff_i *pff_mp) {
        struct list_head      * p;
        struct list_head      * h;
        size_t                  i;
        struct htable_entry   * entry;

        assert(pff_mp);

        htable_for_each(p, i, h, pff_mp->table) {
                entry = list_entry(p, struct htable_entry, next);
                mp_pff_flush(entry->val);
        }
}

int mpi_pff_nhop(struct pff_i *pff_mp,
                 uint64_t addr,
                 int fd_i) {
        struct pff_i *  pff_h;
        int             fd = -1 ;
        size_t          len;

        assert(pff_mp);

        pthread_rwlock_rdlock(&pff_mp->lock);

        if(fd_i < PROG_RES_FDS)
                fd_i = 0;

        if(!htable_lookup(pff_mp->table, fd_i, (void **) &pff_h, &len))
                fd = mp_pff_nhop(pff_h, addr, fd_i);

        pthread_rwlock_unlock(&pff_mp->lock);

        return fd;
}
