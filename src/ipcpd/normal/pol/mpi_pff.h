/*
 * Ouroboros - Copyright (C) 2016 - 2018
 *
 * Simple policy for PFF
 *
 *    Dimitri Staessens <dimitri.staessens@ugent.be>
 *    Sander Vrijders   <sander.vrijders@ugent.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., http://www.fsf.org/about/contact/.
 */

#ifndef OUROBOROS_MPI_PFF_H
#define OUROBOROS_MPI_PFF_H

#include "pol-pff-ops.h"
#include "ouroboros/list.h"

struct pff_i * mpi_pff_create(void);

void           mpi_pff_destroy(struct pff_i * pff_i);

void           mpi_pff_lock(struct pff_i * pff_i);

void           mpi_pff_unlock(struct pff_i * pff_i);

int            mpi_pff_add(struct pff_i * pff_i,
                           uint64_t       addr,
                           int *          fd,
                           int          fd_i,
                           size_t         len);

int            mpi_pff_update(struct pff_i * pff_i,
                              uint64_t       addr,
                              int *          fd,
                              int          fd_i,
                              size_t         len);

int            mpi_pff_del(struct pff_i * pff_i,
                           uint64_t       addr);

void           mpi_pff_flush(struct pff_i * pff_i);

/* Returns fd towards next hop */
int            mpi_pff_nhop(struct pff_i * pff_i,
                            uint64_t       addr,
                            int            fd_i);

struct pol_pff_ops mpi_pff_ops;

#endif /* OUROBOROS_IPCPD_NORMAL_MPI_PFF_H */