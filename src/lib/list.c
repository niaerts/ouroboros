/*
 * Ouroboros - Copyright (C) 2016 - 2018
 *
 * Simple doubly linked list implementation.
 *
 *    Dimitri Staessens <dimitri.staessens@ugent.be>
 *    Sander Vrijders   <sander.vrijders@ugent.be>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., http://www.fsf.org/about/contact/.
 */

#include <ouroboros/list.h>

#include <stddef.h>
#include <malloc.h>

void list_head_init(struct list_head * h)
{
        h->nxt = h;
        h->prv = h;
}

static void add_list(struct list_head * n,
                     struct list_head * prv,
                     struct list_head * nxt)
{
        nxt->prv = n;
        n->nxt = nxt;
        n->prv = prv;
        prv->nxt = n;
}

static void del_list(struct list_head * prv,
                     struct list_head * nxt)
{
        nxt->prv = prv;
        prv->nxt = nxt;
}

void list_add(struct list_head * n,
              struct list_head * h)
{
        add_list(n, h, h->nxt);
}

void list_add_tail(struct list_head * n,
                   struct list_head * h)
{
        add_list(n, h->prv, h);
}

void list_del(struct list_head * e)
{
        del_list(e->prv, e->nxt);
        e->nxt = e->prv = NULL;
}

void list_destroy(struct list_head * e, void (* destructor)(void *)){
        struct list_head * p, * t;

        if(destructor == NULL)
                destructor = free;

        list_for_each_safe(p, t, e) {
                destructor(p);
        }

        destructor(e);
}

bool list_is_empty(struct list_head * h)
{
        return h->nxt == h;
}

void list_move(struct list_head * n,
               struct list_head * h)
{
        del_list(n->prv, n->nxt);
        add_list(n, h, h->nxt);
}
